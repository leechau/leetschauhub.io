<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title><![CDATA[Category: Ftp | Dark Matter in Cyberspace]]></title>
  <link href="http://leetschau.github.io/blog/categories/ftp/atom.xml" rel="self"/>
  <link href="http://leetschau.github.io/"/>
  <updated>2016-02-21T11:06:40+08:00</updated>
  <id>http://leetschau.github.io/</id>
  <author>
    <name><![CDATA[Li Chao]]></name>
    
  </author>
  <generator uri="http://octopress.org/">Octopress</generator>

  
  <entry>
    <title type="html"><![CDATA[Setup Lightweight Python FTP Server for Test]]></title>
    <link href="http://leetschau.github.io/blog/2014/01/24/095359/"/>
    <updated>2014-01-24T09:53:59+08:00</updated>
    <id>http://leetschau.github.io/blog/2014/01/24/095359</id>
    <content type="html"><![CDATA[<p>Based on <a href="http://code.google.com/p/pyftpdlib/">Python FTP server library (pyftpdlib)</a></p>

<h1>Install</h1>

<ol>
<li><p>Start a virtual environment;</p></li>
<li><p>$ easy_install pyftpdlib (the version number is 1.3.0)</p></li>
</ol>


<p>or download pyftpdlib-1.3.0.tar.gz manually and &ldquo;python setup.py install&rdquo;.</p>

<h1>Usage</h1>

<p>Run the following script to startup server:</p>

<pre><code>from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
authorizer = DummyAuthorizer()
authorizer.add_user("user", "12345", "/home/chad/tmp", perm="elradfmw")
authorizer.add_anonymous("/home/chad/tmp2")
# you have a named and an anonymous user to login
handler = FTPHandler
handler.authorizer = authorizer
server = FTPServer(("0.0.0.0", 2121), handler)
server.serve_forever()
</code></pre>

<p>If you set the port to &ldquo;21&rdquo;, a &ldquo;permission denied&rdquo; error raises. So I changed it to 2121.
If you only accept local connection, modify server ip as &ldquo;127.0.0.1&rdquo; instead of &ldquo;0.0.0.0&rdquo;.</p>

<p>Use this server in command line:</p>

<pre><code>ftp 10.21.2.7 2121
// user name and password
pwd
ls
get file1.txt
put file2.txt
</code></pre>

<p>And a groovy ftp client:</p>

<pre><code>import org.apache.commons.net.ftp.FTPClient
println("About to connect....");
new FTPClient().with {
    connect "localhost", 2121
    login "user", "123"
    def names = listNames("/")
    print names
    logout()
    disconnect()
}
println("Done.");
</code></pre>

<p>Run this client:</p>

<pre><code>groovy -cp .../commons-net-3.1.jar ftpclient.groovy
</code></pre>

<p>On my machine this path is:
~/.m2/repository/commons-net/commons-net/3.1/commons-net-3.1.jar</p>
]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[FTP Server on Linux]]></title>
    <link href="http://leetschau.github.io/blog/2013/10/24/094321/"/>
    <updated>2013-10-24T09:43:21+08:00</updated>
    <id>http://leetschau.github.io/blog/2013/10/24/094321</id>
    <content type="html"><![CDATA[<h1>Ubuntu</h1>

<ol>
<li><p>Install ftp server: sudo apt-get install vsftpd;</p></li>
<li><p>Configuration: modify /etc/vsftpd.conf:</p>

<ol type="a">
<li><p>Enable upload: uncomment &ldquo;write_enable=YES&rdquo;</p></li>
<li><p>Restrict user in their home directory: uncomment following lines:</p>

<p> chroot_local_user=YES
 chroot_list_enable=YES
 chroot_list_files=/etc/vsftpd.chroot_list</p></li>
</ol>


<p> then create a /etc/vsftpd.chroot_list containing a list of users one per line.</p></li>
<li><p>Restart vsftpd: sudo /etc/init.d/vsftpd restart</p></li>
</ol>


<p>Ref:</p>

<p><a href="https://help.ubuntu.com/10.04/serverguide/ftp-server.html">https://help.ubuntu.com/10.04/serverguide/ftp-server.html</a>
<a href="http://serverfault.com/questions/354671/ubuntu-vsftpd-server-error-500-oops-could-not-read-chroot-list-file-etc-vsf">http://serverfault.com/questions/354671/ubuntu-vsftpd-server-error-500-oops-could-not-read-chroot-list-file-etc-vsf</a></p>

<h1>CentOS</h1>

<p>See note &ldquo;Install FTP Service for CentOS 6.3 Host behind a Proxy&rdquo;.</p>
]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Install FTP Service for CentOS Host Behind a Proxy]]></title>
    <link href="http://leetschau.github.io/blog/2013/09/09/113239/"/>
    <updated>2013-09-09T11:32:39+08:00</updated>
    <id>http://leetschau.github.io/blog/2013/09/09/113239</id>
    <content type="html"><![CDATA[<h1>Basic Installation</h1>

<ol>
<li><p>add &ldquo;proxy=<a href="http://10.21.3.31:8087">http://10.21.3.31:8087</a>&rdquo; to /etc/yum.conf;</p></li>
<li><p>run command &ldquo;yum install -y vsftpd&rdquo; with root;</p></li>
<li><p>run &ldquo;service vsftpd start&rdquo; with root. Now you can use ftp with a common user;</p></li>
</ol>


<h1>Enable upload and create directory</h1>

<p>Run following command with root accout (or there will raise 553 and 550 errors when you upload and create dir):</p>

<pre><code>setsebool allow_ftpd_full_access on
setsebool ftp_home_dir on
/sbin/service vsftpd restart
</code></pre>

<p>Verify:</p>

<pre><code>$ nc -zv 10.0.2.49 21
</code></pre>

<p>or</p>

<pre><code>$ ftp 10.0.2.49
</code></pre>

<p>then enter username and password. Any existing user account can be used.</p>

<p>If you get following errors, it&rsquo;s probably due to the fiirwall. See the next section for solutions.</p>

<pre><code>nc: connect to 10.0.2.49 port 21 (tcp) failed: No route to host
</code></pre>

<p>Ref:</p>

<p>google &ldquo;vsftpd 553 centos&rdquo;;
<a href="https://www.centos.org/docs/5/html/Deployment_Guide-en-US/s1-ftp-vsftpd-start.html">Starting and Stopping vsftpd</a></p>

<h1>Config Firewall</h1>

<p>First you should test if the connection failure is caused by iptables:</p>

<pre><code># service iptables stop
</code></pre>

<p>and connect the ftp again. If you can logged in, then you should config iptables rules.</p>

<p>Add a rule in iptables to open access for port 21:</p>

<p>Open file /etc/sysconfig/iptables:</p>

<p>Add &ldquo;-A INPUT -i eth0 -p tcp &ndash;dport 21 -j ACCEPT&rdquo; after the line &ldquo;-A INPUT -i lo -j ACCEPT&rdquo;. Then restart service:</p>

<pre><code># service iptables restart
</code></pre>

<p>Verify again.</p>
]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Notes About Putty & Kitty]]></title>
    <link href="http://leetschau.github.io/blog/2012/12/24/164044/"/>
    <updated>2012-12-24T16:40:44+08:00</updated>
    <id>http://leetschau.github.io/blog/2012/12/24/164044</id>
    <content type="html"><![CDATA[<p>Kitty是putty的portable版本，所有配置保存在文本文件中（putty保存在注册表中），新建kitty session比较好的方法是复制一份DefualtSettings文件，修改其中的字段，而不通过kitty自己的配置界面，这个界面用起来很不方便，下面是配置过程：</p>

<ol>
<li>定制默认配置：字体为11号Consolas，终端24行80列，</li>
</ol>


<p> Font\Consolas\</p>

<p> FontHeight\11\</p>

<p> TermHeight\24\
 TermWidth\80\</p>

<ol>
<li>复制文件DefaultSettings为747：ssh lichao@10.0.7.47，端口号22，连接后自动运行脚本d:\apps\Kitty\Scripts\cat47.txt（转义字符的写法是%+16进制的ASCII码，例如"\&ldquo;是%5C，&rdquo;:&ldquo;是%3A）</li>
</ol>


<p> HostName\10.0.7.47\</p>

<p> Protocol\ssh\</p>

<p> PortNumber\22\</p>

<p> UserName\lichao\
 Scriptfile\D%3A%5CApps%5CKitty%5CScripts%5Ccat47.txt\</p>

<ol>
<li><p>编写自动运行脚本（如果需要的话）：格式是一行预期一行发送（见笔记“ Notes about Kitty ”）；</p></li>
<li><p>保持密码（可选）：启动配置界面（kitty.exe），加载747，输入登录密码（密码是加密后存入配置文件的，无法直接在配置文件里写），保存747；</p></li>
</ol>


<h1>切换全屏模式</h1>

<p>Ctrl + right click，点击菜单切换全屏，当连接的"FullScreenOnAltEnter"设置为true时，也可以用Alt+Enter切换（在配置界面上的位置是Window->Behavior最后一项，或者session文件中" FullScreenOnAltEnter\1\ &ldquo;）；</p>

<h1>连接断开后自动关闭窗口</h1>

<p>session文件中"CloseOnExit\1\&ldquo;表示自动关闭，"CloseOnExit\0\"表示不自动关闭；</p>

<p>&ldquo;AltF4\1\"表示可以用Alt+F4关闭窗口，0表示此组合键无效；</p>

<h1>TCP连接</h1>

<p>putty.exe -raw <ip> -P <port></p>

<p>例如：putty -raw 10.31.1.23 -P 3128</p>

<h1>SSH自动登录</h1>

<p> putty.exe -ssh -pw mypassword <a href="&#109;&#97;&#105;&#108;&#x74;&#x6f;&#58;&#117;&#115;&#x65;&#114;&#110;&#x61;&#x6d;&#101;&#64;&#49;&#48;&#46;&#x30;&#46;&#50;&#46;&#54;&#x38;">&#x75;&#115;&#101;&#114;&#x6e;&#97;&#109;&#x65;&#64;&#x31;&#x30;&#x2e;&#x30;&#x2e;&#x32;&#46;&#x36;&#x38;</a></p>

<p>参考<a href="http://superuser.com/questions/44106/is-there-a-way-to-auto-login-in-putty-with-a-password">http://superuser.com/questions/44106/is-there-a-way-to-auto-login-in-putty-with-a-password</a></p>

<h1>文件传输</h1>

<h2>一次性传输：pscp</h2>

<h3>上传文件</h3>

<ul>
<li>将f:\hw.txt上传到219的/home/chad目录下：</li>
</ul>


<p> pscp -pw mypwd f:*.txt chad@10.31.1.219:/home/chad/</p>

<h3>下载文件</h3>

<ul>
<li>将96上的/home/chad/hw.txt文件下载到f:\下：</li>
</ul>


<p> pscp -pw mypwd chad@10.31.1.96:home/chad/*.txt f:\</p>

<ul>
<li>将113上autoClient文件夹下所有文件拷贝到当前目录下：</li>
</ul>


<p> pscp -pw mypwd lichao@10.31.1.113:/home/lichao/autoClient/* ./autoClient/</p>

<p>注意本地的文件夹autoClient必须存在（pscp不能自己创建文件夹），且pscp不支持/<em>*/</em>这样的多级目录拷贝；autoClient后面的/必须加，否则会把autoClient当成拷贝文件的新名字；</p>

<p>这两种指令都可以在目标路径后面写上文件名，达到修改文件名的目的；</p>

<p>参考：putty文档 chapter 5.</p>

<h1>交互式传输：psftp</h1>

<ul>
<li><p>登录服务器：psftp -pw mypwd <a href="&#109;&#97;&#x69;&#108;&#116;&#x6f;&#58;&#117;&#115;&#101;&#114;&#110;&#97;&#109;&#101;&#64;&#49;&#48;&#46;&#x32;&#x2e;&#51;&#46;&#53;&#x36;">&#x75;&#x73;&#101;&#x72;&#x6e;&#x61;&#x6d;&#x65;&#x40;&#x31;&#48;&#46;&#x32;&#46;&#51;&#46;&#53;&#x36;</a></p></li>
<li><p>查询服务器当前目录：pwd</p></li>
<li><p>改变服务器当前目录：cd &hellip;</p></li>
<li><p>查询和改变本地目录：lpwd, lcd</p></li>
<li><p>上传：put f:\hw.txt</p></li>
<li><p>下载：get hw.txt</p></li>
<li><p>运行本地命令：!+cmd_name，例如!dir显示本地文件，!type Readme.txt显示Readme.txt文件内容；</p></li>
<li><p>上传/下载多个文件：mput/mget;</p></li>
<li><p>psftp.exe的自动执行脚本：psftp <a href="&#109;&#97;&#x69;&#108;&#x74;&#x6f;&#x3a;&#117;&#x73;&#x65;&#114;&#x40;&#x31;&#x30;&#x2e;&#x30;&#46;&#50;&#x2e;&#52;&#x37;">&#117;&#x73;&#x65;&#114;&#x40;&#x31;&#x30;&#x2e;&#48;&#46;&#x32;&#46;&#x34;&#x37;</a> -bc -b myscript，（相对于putty的-m参数）</p></li>
</ul>


<p>myscript文件内容是自动执行的指令，例如：</p>

<p> cd /home/ftp/users/jeff
 del jam-old.tar.gz
 ren jam.tar.gz jam-old.tar.gz
 put jam.tar.gz</p>

<p>详见putty文档6.2节；</p>

<h1>设置Putty默认属性</h1>

<p>在$PUTTY_HOME\sessions\下有一个%C4%AC%C8%CF%C9%E8%D6%C3文件，其中C4AC C8CF C9E8 D6C3是“默认设置”的Unicode编码，这个文件里保存的是默认设置，设置默认设置的好处是以后凡不指明都是用默认值（对于开关选项，0表示关闭，1表示开启）：</p>

<p> Font\Lucida%20Console\
 LineCodePage\UTF-8\
 ScrollbackLines\20000\ ; 缓冲区行数
 Protocol\ssh\ ; 默认连接协议，可以不用-ssh参数特别说明
 TerminalType\xterm\</p>

<p> FullScreenOnAltEnter\1\ ; 按Alt+Enter进入全屏模式
 CloseOnExit\0\ ; 退出网元后不关闭窗口（改为1是关闭窗口）
 MouseIsXterm\0\ ; unix-style Copy/Paste: 左键选择，右键粘贴</p>

<h1>使用方法</h1>

<p>对于常用连接，可以写成一个bat脚本，用Executor做快捷键启动；对于临时连接，可以手工敲开始处的ssh自动登录脚本连接。</p>

<h2>putty无法自动登录问题</h2>

<p>现象：telnet无法自动登录：putty -telnet user@10.0.2.34，然后手工输入密码。</p>

<p>解决方法：使用kitty_portable代替putty，它可以输入登录密码，然后把自动登录服务器保存为一个session，例如名为mysession，然后用"kitty -load mysession"启动之。</p>

<p>putty也有-load参数，但它的session信息是保存在注册表里，portable能力稍差。</p>

<p>参考：</p>

<ul>
<li><p><a href="http://dag.wieers.com/blog/content/improving-putty-settings-on-windows">Improving Putty settings on Windows</a></p></li>
<li><p><a href="http://heroxuan.iteye.com/blog/560682">精彩PuTTY 中文教程</a></p></li>
</ul>

]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Windows Server FTP工具]]></title>
    <link href="http://leetschau.github.io/blog/2012/11/19/145129/"/>
    <updated>2012-11-19T14:51:29+08:00</updated>
    <id>http://leetschau.github.io/blog/2012/11/19/145129</id>
    <content type="html"><![CDATA[<p>试用了<a href="http://www.freesshd.com/">freeFTPd</a>和<a href="http://www.coreftp.com/server/">mini sftp server on CoreFTP</a>。</p>

<p>freeFTPd的ftp server启动并上传/下载成功，sftp server能启动，也能接收用户登录，但无法上传/下载文件。</p>

<p>mini sftp server是一个绿色文件，只要在启动界面上配好用户名、密码、端口即可，非常方便，只能配一个用户，但这个用户可以与server建立多个连接，用ftp客户端上传/下载正常，但用Ant的scp指令上传文件时，在连接server时僵死。</p>

<p>总结：freeFTPd需要安装，还会生成一个服务，对系统影响比较大，如果只是用sftp做部署时上传的话，mini sftp够用了。</p>

<p>另：freeSSHd启动后用户无法登录，Windows对SSH以及SFTP的支持不好，需要测SSH服务的时候最好在Linux server上测。</p>
]]></content>
  </entry>
  
</feed>
